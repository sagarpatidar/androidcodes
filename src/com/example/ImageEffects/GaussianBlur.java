/**
 *	GaussianBlur.java
 *  Created by Sagar Patidar on 11-Aug-2014, 10:59:14 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

/**
 * 
 */
package com.example.ImageEffects;

/**
 * @author Sagar Patidar
 *
 */

import android.graphics.Bitmap;

public class GaussianBlur implements IAndroidFilter{

    @Override
    public AndroidImage process(AndroidImage imageIn) {
        // TODO Auto-generated method stub
        Bitmap src=imageIn.getImage();
        double[][] GaussianBlurConfig = new double[][] {
                { 1, 2, 1},
                { 2, 4, 2},
                { 1, 2, 1}
            };
            ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
            convMatrix.applyConfig(GaussianBlurConfig);
            convMatrix.Factor = 30;
            convMatrix.Offset = 18;
            return new AndroidImage(ConvolutionMatrix.computeConvolution3x3(src, convMatrix));
    }


}
