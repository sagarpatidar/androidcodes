/**
 * @author Sagar Patidar
 *
 */
package com.example.ImageEffects;

// To create various Image Effects like Instagram or any other image editor.

/*
 * AndroidImage im = new AndroidImage(Bitmap bitmap);
 * GaussianBlur effect = new GaussianBlur();
 * AndroidImage im2 = effect.process(im); 
 * Bitmap image = im2.getImage();
 * 
 */

// Use this 'image' anywhere
// changing 'GaussianBlurConfig' Matrix in GaussianBlur.java will change effect on image