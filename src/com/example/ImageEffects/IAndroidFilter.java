/**
 *	IAndroidFilter.java
 *  Created by Sagar Patidar on 11-Aug-2014, 11:04:41 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package com.example.ImageEffects;

public interface IAndroidFilter {

    public AndroidImage process(AndroidImage imageIn);
}
