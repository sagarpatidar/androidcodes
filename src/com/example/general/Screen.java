/**
 *	Screen.java
 *  Created by Sagar Patidar on Sep 28, 2014, 12:48:22 PM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package com.example.general;

import android.app.Activity;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.util.Log;

public class Screen {

	public static void detectDensity(Activity activity){

		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		switch(metrics.densityDpi){
		     case DisplayMetrics.DENSITY_LOW:
		    	 Log.i("DPI", "LOW");
		                break;
		     case DisplayMetrics.DENSITY_MEDIUM:
		    	 Log.i("DPI", "MED");
		                 break;
		     case DisplayMetrics.DENSITY_HIGH:
		    	 Log.i("DPI", "HIGH");
		                 break;
		     case DisplayMetrics.DENSITY_XHIGH:
		    	 Log.i("DPI", "X-HIGH");
		                 break;  
		     case DisplayMetrics.DENSITY_XXHIGH:
		    	 Log.i("DPI", "XX-HIGH");
		                 break;
		     default:
		    	 Log.i("DPI", "NULL");
                 break;
		}
	}
	
	public static void detectScreenSize(Activity activity){

		final String SCREEN_SIZE = "SCREEN SIZE"; 
		switch ((activity.getResources().getConfiguration().screenLayout & 
			    Configuration.SCREENLAYOUT_SIZE_MASK)) {
	    case Configuration.SCREENLAYOUT_SIZE_SMALL:
			Log.i(SCREEN_SIZE, "Small screen");
			break;
	    case Configuration.SCREENLAYOUT_SIZE_NORMAL:
	    	Log.i(SCREEN_SIZE, "normal screen");
			break;
		case Configuration.SCREENLAYOUT_SIZE_LARGE:
			Log.i(SCREEN_SIZE, "large screen");
			break;
		case Configuration.SCREENLAYOUT_SIZE_XLARGE:
			Log.i(SCREEN_SIZE, "xlarge screen");
			break;
		case Configuration.SCREENLAYOUT_SIZE_UNDEFINED:
			Log.i(SCREEN_SIZE, "undefined screen");
			break;
		default:
			Log.i(SCREEN_SIZE, "NULL");
			break;
		}
	}
	
}
