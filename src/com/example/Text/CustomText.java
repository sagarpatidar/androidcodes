/**
 *	CustomText.java
 *  Created by Sagar Patidar on 12-Aug-2014, 2:54:27 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package com.example.Text;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.text.Html;
import android.widget.EditText;
import android.widget.TextView;

public class CustomText {

	// the file 'customFont' must be present under "assets/fonts"
	public static final String CUSTOM_FONT = "fonts/customFont.ttf"; 
	
	public void setFormattedText(TextView tv, String htmlString){
		// this can help in writing a custom formatted text in single textView
		tv.setText(Html.fromHtml(htmlString));
	}
	
	public static void setTextViewFontStyle(AssetManager asset,TextView...params ){
		Typeface tf = Typeface.createFromAsset(asset, CUSTOM_FONT);
		for(TextView v: params){
			v.setTypeface(tf);
		}
	}
	
	public static void setEditTextFontStyle(AssetManager asset,EditText...params ){
		Typeface tf = Typeface.createFromAsset(asset, CUSTOM_FONT);
		for(EditText v: params){
			v.setTypeface(tf);
		}
	}
	
}
